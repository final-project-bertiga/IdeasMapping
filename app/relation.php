<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relation extends Model
{
    //
    protected $table = 'relation';

    protected $fillable = [
        'user_id','idea_id','mainIdea','relation_from', 'relation_to', 'status'
    ];

    /**
     * Get the user that owns the note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function idea()
    {
        return $this->belongsTo('App\idea');    
    }

    public function mainidea()
    {
        return $this->belongsTo('App\idea','mainIdea');    
    }
}
