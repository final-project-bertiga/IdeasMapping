<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sugestion extends Model
{
    //
    protected $table = 'sugestions';
    protected $fillable = [
        'user_id','suggest_to','suggest_from','description'
    ];

    /**
     * Get the user that owns the note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
