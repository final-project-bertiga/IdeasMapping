<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\relation;
use App\idea;
use App\sugestion;
use App\profile;

class relationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $relation = idea::where('id', $id)->firstOrFail();
        return view('relation.home',compact('ideas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $userLogin = Auth::id();
        $countData = relation::count();
        $ideas = idea::all();
        $relation = DB::table("relation")
            ->orderBy('relation.relation_from','asc')
            ->where("mainIdea","=",$id)
            ->join("idea","relation.idea_id","idea.id")
            ->get();
        if ($relation->count()>0) {
        } else {
            $StoreRelation = new relation;
            $StoreRelation -> user_id = $userLogin;
            $StoreRelation -> idea_id = $id;
            $StoreRelation -> relation_from = $countData+1;
            $StoreRelation -> mainIdea = $id;
            $StoreRelation->save();

            $relation = DB::table("relation")
                ->orderBy('relation.relation_from','asc')
                ->where("mainIdea","=",$id)
                ->join("idea","relation.idea_id","idea.id")
                ->get();
        }
        $pro = profile::where('user_id',$userLogin)
            ->first();
        $sugestion = DB::table("sugestions")
            ->join("idea","sugestions.suggest_from","=","idea.id")
            ->join("users","sugestions.user_id","=","users.id")
            ->where("suggest_to","=",$id)
            ->get();
        $SugesRelation = DB::table("relation")
            ->where("mainIdea","=",$id)
            ->get();
        return view('idea.show', array('sugestion'=>$sugestion, 
                                        'relations'=>$relation, 
                                        'ideas'=>$ideas, 
                                        'profile'=>$pro));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accept($id)
    {
        $userLogin = Auth::id();
        
    }
}
