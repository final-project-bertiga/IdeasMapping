<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect("/Idea");
        }
        $ideas = DB::table('idea')
            ->orderBy('idea.claps','desc')
            ->join('users','idea.user_id', '=', 'users.id')
            ->get();
        $ideasDate = DB::table('idea')
            ->orderBy('idea.created_at','desc')
            ->join('users','idea.user_id', '=', 'users.id')
            ->get();
        return view('welcome', ['ideas' => $ideas],  ['ideasDate' => $ideasDate]);
    }
}
