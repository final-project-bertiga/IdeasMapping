<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\idea;
use App\User;
use App\clap;
use App\profile;

class ideacontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = profile::where('user_id',Auth::id())
            ->get();
        if ($profile->count()<=0) {
            $newProfile = new profile;
            $newProfile -> user_id = Auth::id();
            $newProfile -> save();            
        }
        
        $ideas = idea::all()->sortByDesc("claps");
        $ideasDate = idea::all()->sortByDesc("created_at");
        $claps = clap::all();

        return view('idea.home',compact('ideas','claps','ideasDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('idea.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $idea = new idea;
        $idea -> title = $request -> title;
        $idea -> content = $request -> content;
        $idea -> user_id = $request -> user_id;

        $idea -> save();

        return redirect('/Idea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $idea = idea ::find($id);
        return view::make('idea.edit')
            ->with('idea', $idea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $idea = idea::find($id);
        $idea->title        = $request->title;
        $idea->content      = $request->content;
        $idea->save();

        return redirect('/Idea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $idea = idea::find($id);
        $idea->delete();

        return redirect('/Idea');
    }

    public function addClaps($id)
    {
        $userLogin = Auth::id();
        $userClaps = clap::where('user_id' , $userLogin)
            ->where('clapTo' , $id)->first();
        if (empty($userClaps->user_id)) {
            $NewClap = new clap;
            $NewClap->user_id   = $userLogin;
            $NewClap->clapTo    = $id;
            $NewClap->save();

            $idea = idea::find($id);
            $idea->claps      = $idea->claps+1;
            $idea->save();

            return redirect('/Idea');
        } else {
            $DelClaps = clap::where('user_id' , $userLogin)
                ->where('clapTo' , $id)->first();
            $DelClaps->delete();

            $idea = idea::find($id);
            $idea->claps      = $idea->claps-1;
            $idea->save();

            return redirect('/Idea');
        }
    }

    public function permission($id)
    {
        # code...
        $idea = idea::find($id);
        if ($idea->status==0) {
            # code...
            $idea->status = 1;
            $idea->save();
            return redirect('/Idea');
        }
        if ($idea->status==1){
            # code...
            $idea->status = 0;
            $idea->save();
            return redirect('/Idea');
        }
    }
}
