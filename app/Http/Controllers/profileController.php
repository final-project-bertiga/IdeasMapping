<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\profile;
use App\subscribe;

class profileController extends Controller
{
    public function index($id)
    {
        $userLogin = Auth::id();
    	$prof = profile::where('user_id' , $userLogin);
    	$listenTo = subscribe::where('user_id' , $userLogin);
    	$subs = subscribe::where('listenTo',$id);

        return view('profile.index',compact('prof','listenTo','subs'));
    }

    public function subs($id)
    {
        $userLogin = Auth::id();
    	$subsThis = subscribe::where('user_id' , $userLogin)
    		->where('listenTo',$id)->first();
    	if (empty($subsThis->user_id)) {
    		$subs = new subscribe;
    		$subs->user_id = $userLogin;
    		$subs->listenTo = $id;
    		$subs->save();
    		$prof = profile::find($id);
    		$prof->totalSubs =  $prof->totalSubs + 1;
    		$prof->save();

    	} else {
    		$prof = profile::find($id);
    		$prof->totalSubs =  $prof->totalSubs - 1;
    		$prof->save();

    		$delSubs = subscribe::where('user_id' , $userLogin)
    			->where('listenTo',$id);
    		$delSubs->delete();
    	}
    	return redirect('/Profile/'.$id);
    }
}
