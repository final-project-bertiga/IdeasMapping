<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class idea extends Model
{
    //
    protected $table = 'idea';

    protected $fillable = [
        'user_id', 'title', 'content', 'status', 'up_vote',
    ];

    /**
     * Get the user that owns the note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}