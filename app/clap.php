<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clap extends Model
{
    //
    protected $table = 'clap';

    protected $fillable = [
        'user_id','clapTo',
    ];

    /**
     * Get the user that owns the note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function idea()
    {
        return $this->belongsTo('App\idea','clapTo');
    }
}
