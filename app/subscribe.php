<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscribe extends Model
{
    //
    protected $table = 'subscribe';

    protected $fillable = [
        'user_id', 'listenTo',
    ];

    /**
     * Get the user that owns the note.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}