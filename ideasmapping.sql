-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 04, 2018 at 04:03 AM
-- Server version: 5.7.19
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ideasmapping`
--
CREATE DATABASE IF NOT EXISTS `ideasmapping` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ideasmapping`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `CategoryName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clap`
--

DROP TABLE IF EXISTS `clap`;
CREATE TABLE `clap` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `clapTo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clap`
--

INSERT INTO `clap` (`id`, `user_id`, `clapTo`, `created_at`, `updated_at`) VALUES
(5, 1, 1, '2018-03-23 02:03:00', '2018-03-23 02:03:00'),
(19, 2, 3, '2018-05-15 16:21:49', '2018-05-15 16:21:49'),
(20, 2, 2, '2018-05-15 16:47:33', '2018-05-15 16:47:33'),
(24, 1, 2, '2018-05-25 06:48:35', '2018-05-25 06:48:35'),
(25, 1, 5, '2018-05-27 20:47:46', '2018-05-27 20:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `idea`
--

DROP TABLE IF EXISTS `idea`;
CREATE TABLE `idea` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `claps` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `idea`
--

INSERT INTO `idea` (`id`, `user_id`, `title`, `content`, `status`, `claps`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 'thorn', 'Thorn or þorn (Þ, þ) is a letter in the Old English, Gothic, Old Norse and modern Icelandic alphabets, as well as some dialects of Middle English.', 0, 2, '2018-03-23 02:05:47', '2018-05-25 06:48:35', NULL),
(3, 1, 'Ini Title', 'ini isinya Content yang isinya abal abal seperti katamu', 0, 1, '2018-05-04 22:50:31', '2018-05-15 18:00:52', NULL),
(5, 1, 'project management', 'Description', 0, 1, '2018-05-13 21:49:00', '2018-05-27 20:47:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_23_034201_create_idea_table', 1),
(4, '2018_01_23_035706_create_relation_table', 1),
(5, '2018_01_23_052333_create_request_change_table', 1),
(6, '2018_01_26_031305_create_profile_table', 1),
(7, '2018_02_23_045822_create_category_table', 1),
(8, '2018_02_23_065559_create_notifications_table', 1),
(9, '2018_03_19_121855_create_clap_table', 1),
(10, '2018_03_19_123027_create_subscribe_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sidi_meisanjaya@yahoo.com', '$2y$10$Jv1CGpXItClr2RbTndLF2.PZ/fyHAq4Js3GjRkcxQFV6rFO3yP2Ye', '2018-05-15 17:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `totalSubs` int(11) NOT NULL DEFAULT '0',
  `statLevel` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `totalSubs`, `statLevel`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, '2018-05-13 22:14:29', '2018-05-27 21:34:44'),
(2, 2, 0, 0, '2018-05-15 16:21:41', '2018-05-15 16:21:41'),
(3, 3, 0, 0, '2018-05-27 23:26:09', '2018-05-27 23:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `relation`
--

DROP TABLE IF EXISTS `relation`;
CREATE TABLE `relation` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `idea_id` int(11) NOT NULL,
  `mainIdea` int(11) NOT NULL,
  `relation_from` int(11) DEFAULT NULL,
  `relation_to` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `relation`
--

INSERT INTO `relation` (`id`, `user_id`, `idea_id`, `mainIdea`, `relation_from`, `relation_to`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, 4, 1, NULL, 0, '2018-05-08 18:36:33', '2018-05-08 18:36:33', NULL),
(2, 1, 3, 4, 11, 1, 0, '2018-05-11 01:36:28', '2018-05-11 01:36:28', NULL),
(3, 1, 2, 2, 3, NULL, 0, '2018-05-11 01:36:40', '2018-05-11 01:36:40', NULL),
(4, 1, 5, 5, 4, NULL, 0, '2018-05-13 21:50:29', '2018-05-13 21:50:29', NULL),
(5, 1, 3, 3, 5, NULL, 0, '2018-05-27 21:57:18', '2018-05-27 21:57:18', NULL),
(6, 1, 6, 6, 6, NULL, 0, '2018-05-27 21:59:14', '2018-05-27 21:59:14', NULL),
(7, 1, 7, 7, 7, NULL, 0, '2018-05-27 22:01:52', '2018-05-27 22:01:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_change`
--

DROP TABLE IF EXISTS `request_change`;
CREATE TABLE `request_change` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
CREATE TABLE `subscribe` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `listenTo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sugestions`
--

DROP TABLE IF EXISTS `sugestions`;
CREATE TABLE `sugestions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `suggest_to` int(11) NOT NULL,
  `suggest_from` int(11) NOT NULL,
  `description` varchar(5000) NOT NULL DEFAULT 'No Description',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sugestions`
--

INSERT INTO `sugestions` (`id`, `user_id`, `suggest_to`, `suggest_from`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2, 'No Description', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sidisan', 'sidi.meisanjaya2013@gmail.com', '$2y$10$FRob06/AUyQwq5rifhgzu.JdT6xMwlXwmr8lAq7B79luXwtGhh1dW', '5tGN9K0bnDZYrhyfNIYwvHmLW3SY2UK2IyPFAIeXBgx5M4RMpPO3fV8V0NXw', '2018-03-19 22:06:21', '2018-03-19 22:06:21'),
(2, 'SidiYahoo', 'sidi_meisanjaya@yahoo.com', '$2y$10$3fp9nBPZWL2ywskdGLYumu8NL5n2mgFkbHHz29fNbtgrroE1V3Sg2', 'qbj4r8guTv0sjxBvYXMOoKjSFVaMcy3NxttbcV1D29VHwN7VjqiPcqky1US0', '2018-03-23 03:15:08', '2018-05-15 16:21:40'),
(3, 'sidigame', 'sidi.game2018@gmail.com', '$2y$10$W6RigSJ/wZ6TYfNkNkglbO99EZf71zAA4C7FpuK0Q9Kmi4Kfjo8KS', 'N7tNBZu1Lw8LDKwCEgU98KHJy9c0q7OF9xTmQdeNlS3AZxLRc0oHqR3drC3K', '2018-05-27 23:26:09', '2018-05-27 23:26:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clap`
--
ALTER TABLE `clap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idea`
--
ALTER TABLE `idea`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relation`
--
ALTER TABLE `relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_change`
--
ALTER TABLE `request_change`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clap`
--
ALTER TABLE `clap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `idea`
--
ALTER TABLE `idea`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `relation`
--
ALTER TABLE `relation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `request_change`
--
ALTER TABLE `request_change`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
