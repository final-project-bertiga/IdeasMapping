@extends('layouts.mind')

@section('form')
	<div class="section">
        <div class="row">
			<div class="col s12 m5 l8 offset-l2 ">
                <div id="contact-page" class="card">
                    <div class="card-content">
						<form class="contact-form" action="{{url('/Idea',[$idea -> id])}}" method="POST">
							{{ csrf_field() }}
							{{ method_field('PUT') }}
    					  <div class="row">
    						<div class="input-field col s12">
					      		<input type="hidden" name="id" id="id" value="{{$idea -> id}}">
					        	<input type="hidden" name="user_id" id="user_id" value="{{Auth::id()}}">
    						  	<input type="text" name="title" id="title" value="{{$idea -> title}}">
    						  	<label for="title">Title</label>
    						</div>
    					  </div>
    					  <div class="row">
    						<div class="input-field col s12">
					        	<textarea class="materialize-textarea" name="content" id="content" >{{$idea -> content}}</textarea>
    						  	<label for="content">Content</label>
    						</div>
    						<div class="row">
    						  	<div class="input-field col s12">
    								<button class="btn cyan waves-effect waves-light right" type="submit" name="action">Edit
    							  	<i class="mdi-content-send right"></i>
    								</button>
    						  	</div>
    						</div>
    					  </div>
    					</form>
    				</div>
                </div>
			</div>
		</div>            
    </div>
@endsection
