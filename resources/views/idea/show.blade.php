@extends('layouts.mind')

@section('ideaPage')
<textarea id="mySavedModel" style="width:100%;height:400px;display:none;" >{ "class": "go.TreeModel",
  "nodeDataArray": [
{"key":0, "text":"Mind Map", "loc":"0 0"},
{"key":1, "parent":0, "text":"Getting more time", "brush":"skyblue", "dir":"right", "loc":"77 -22"},
{"key":11, "parent":1, "text":"Wake up early", "brush":"skyblue", "dir":"right", "loc":"200 -48"},
{"key":12, "parent":1, "text":"Delegate", "brush":"skyblue", "dir":"right", "loc":"200 -22"},
{"key":13, "parent":1, "text":"Simplify", "brush":"skyblue", "dir":"right", "loc":"200 4"},
{"key":2, "parent":0, "text":"More effective use", "brush":"darkseagreen", "dir":"right", "loc":"77 43"},
{"key":21, "parent":2, "text":"Planning", "brush":"darkseagreen", "dir":"right", "loc":"203 30"},
{"key":211, "parent":21, "text":"Priorities", "brush":"darkseagreen", "dir":"right", "loc":"274 17"},
{"key":212, "parent":21, "text":"Ways to focus", "brush":"darkseagreen", "dir":"right", "loc":"274 43"},
{"key":22, "parent":2, "text":"Goals", "brush":"darkseagreen", "dir":"right", "loc":"203 56"},
{"key":3, "parent":0, "text":"Time wasting", "brush":"palevioletred", "dir":"left", "loc":"-20 -31.75"},
{"key":31, "parent":3, "text":"Too many meetings", "brush":"palevioletred", "dir":"left", "loc":"-117 -64.25"},
{"key":32, "parent":3, "text":"Too much time spent on details", "brush":"palevioletred", "dir":"left", "loc":"-117 -25.25"},
{"key":33, "parent":3, "text":"Message fatigue", "brush":"palevioletred", "dir":"left", "loc":"-117 0.75"},
{"key":331, "parent":31, "text":"Check messages less", "brush":"palevioletred", "dir":"left", "loc":"-251 -77.25"},
{"key":332, "parent":31, "text":"Message filters", "brush":"palevioletred", "dir":"left", "loc":"-251 -51.25"},
{"key":4, "parent":0, "text":"Key issues", "brush":"coral", "dir":"left", "loc":"-20 52.75"},
{"key":41, "parent":4, "text":"Methods", "brush":"coral", "dir":"left", "loc":"-103 26.75"},
{"key":42, "parent":4, "text":"Deadlines", "brush":"coral", "dir":"left", "loc":"-103 52.75"},
{"key":43, "parent":4, "text":"Checkpoints", "brush":"coral", "dir":"left", "loc":"-103 78.75"}
 ]
}
</textarea>
                <div class="section">
                  <div id="contact-page" class="card">
                    <div class="card-image waves-effect waves-block waves-light">
        				      <div id="map-canvas">
                      </div>
                    </div>

                    <div class="divider"></div>
                    <div class="card-content">
                      <a data-activates="chat-out" class="btn-floating activator btn-move-up waves-effect waves-light chat-collapse right">
                          <i class="mdi-navigation-menu"></i>
                      </a>
                      <?php
                      $found=0;
                      foreach ($ideas as $idea) {

                        $x = rand(0,4);
                        $urlID = request()->route('id');
                        $color = array('circle grey darken-1', 'circle indigo darken-1', 'circle light-blue', 'circle light-blue', 'circle red');
                       	if ($idea->id == $urlID) {
                          if ($idea->user_id == Auth::id()) {
                            # code...
                            $found=1;
                          }
                      ?>
                      <div class="row">
                        <div id="email-details" class="col s12 m8 l8 card-panel">
                          <p class="email-subject truncate">{{$idea->title}}
                          </p>
                          <hr class="grey-text text-lighten-2">
                          <div class="email-content-wrap">
                            <div class="row">
                              <div class="col s10 m10 l10">
                                <ul class="collection">
                                  <li class="collection-item avatar">
                                    <span class="{{$color[$x]}}">{{$idea->user->name[0]}}</span>
                                    <span class="email-title">{{$idea->user->name}}</span>
                                    <p class="grey-text ultra-small">{{$idea->created_at}}</p>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="email-content">
                              {{$idea -> content}}
                            </div>
                          </div>
                        </div>
                        <?php
                        	break;
                    	  }
                      } ?>


                        <div class="col s12 m3 l3">
                          <!--Input Switches-->
                          <div id="input-switches">
            							  <div class="collection">
                              @if($found>0)
                              <a href="{{url('/Idea/'.$urlID.'/edit')}}" class="collection-item">
                                <i class="mdi-communication-email">
                                </i> Edit Idea
                              </a>
                              <a href="{{url('/Idea/'.$urlID.'/delete')}}" class="collection-item"><i class="mdi-action-delete">
                                </i> Delete Idea
                              </a>
                              @else
                              <a href="#!" class="collection-item"><i class="mdi-content-report">
                                </i> Report this Idea
                              </a>
                              <a href="#!" class="collection-item"><i class="mdi-communication-email">
                                </i> Suggest to edit Idea
                              </a>
                              <a href="{{URL('/Idea/sugestion/'.$urlID.'/To')}}" class="collection-item"><i class="mdi-editor-insert-link">
                                </i> Suggest Link to an Idea
                              </a>
                              @endif
            							  </div>
                          </div>
                        </div>
                      </div>

			                <div class="row">
			                  <div id="email-details" class="col s12 m6 l8 card-panel">
                          <p class="email-subject truncate">
                            Suggestions for this idea
                          </p>
                          <hr class="grey-text text-lighten-2">
                          @foreach ($sugestion as $suges)
                          <?php $x = rand(0,4); ?>
                          <div class="email-content-wrap">
                            <div class="row">
                              <div class="col s10 m10 l9">
                                <ul class="collection">
                                  <li class="collection-item avatar">
                                    <span class="{{$color[$x]}}">{{$suges->name[0]}}</span>
                                    <span class="email-title">{{$suges->name}}</span>
                                  </li>
                                </ul>
                              </div>
                              <div class="col s3 m3 l3 email-actions">
                                @if($found>0)
                                <a href="{{url('/Idea/sugestion/' . $suges->id . '/accept')}}"><span><i class="mdi-action-spellcheck"></i></span> Accept</a>
                                <a href="{{url('/Idea/sugestion/' . $suges->id . '/decline')}}"><span><i class="mdi-action-highlight-remove"></i></span> Decline</a>
                                @endif
                              </div>
                            </div>
                            <div class="email-content">
                              <h6>Change title to: {{$suges->title}}</h6>
                              <div class="divider"></div>
                              <p>Change Content to:</p>
                              <p>{{$suges->content}}</p>
                            </div>
        					        </div><br><hr>
                          @endforeach
                          <br><hr>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
@endsection

@section('rightSideNav')
			<aside id="right-sidebar-nav">
              <ul id="chat-out" class="side-nav rightside-navigation">
                <li class="li-hover">
                  <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                </li>
                <li class="li-hover">
                  <ul class="chat-collapsible" data-collapsible="expandable">
                    <?php $count=0; ?>
                    @foreach ($relations as $relate)
                    <li>
                      <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>{{$relate->title}}:{{$relate->relation_from}}</div>
                      <div class="collapsible-body recent-activity">
                        <div class="recent-activity-list chat-out-list row">
                          <div class="col s9 recent-activity-list-text">
                            <?php
                            if ($relate->mainIdea==$relate->idea_id) {
                            ?>
                            <a href="{{URL::to('/Idea/'.$relate->idea_id.'/relations')}}">
                            <?php
                            } else {
                            ?>
                            <a href="{{URL::to('/Idea/'.$relate->mainIdea.'/'.$relate->idea_id.'/relations')}}">
                            <?php
                            }
                            ?>{{$relate->title}}:{{$relate->relation_from}}</a>
                          </div>
                        </div>
                        @foreach ($relations as $child)
                        <?php
                        if ($child->relation_to==$relate->relation_from) {
                        ?>
                        <div class="recent-activity-list chat-out-list row">
                          <div class="col s9 recent-activity-list-text">
                            <a href="#">{{$child->title}}:{{$child->relation_from}}</a>
                          </div>
                        </div>
                      <?php } ?>
                        @endforeach
                      </div>
                    </li>
                    @endforeach
                  </ul>
                </li>
              </ul>
            </aside>
@endsection
