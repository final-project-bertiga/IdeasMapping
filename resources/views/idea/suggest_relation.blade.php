@extends('layouts.mind')

@section('form')

<div class="section">
	<div class="row">
	<div class="col s12 m5 l8 offset-l2 ">
	<div id="contact-page" class="card">
		<div class="card-content">
			<h4 class="header">suggestion for relationship</h4>
			<form class="contact-form">
			  <div id="input-select" class="row">
				<div class="col s12 m8 l9">
				  <div class="input-field col s12">
					<label>Select Parent Idea</label>
					<select>
					  <option value="" disabled selected>Choose Parent Idea</option>
					  <option value="1">Mind Map</option>
					</select>
				  </div>
				</div>
			  </div>

			  <div id="input-select" class="row">
				<div class="col s12 m8 l9">
				  <div class="input-field col s12">
					<label>Select Child Idea</label>
					<select>
					  <option value="" disabled selected>Choose Child Idea</option>
					  <option value="1">Time wasting</option>
					</select>
				  </div>
				</div>
			  </div>
				<div class="row">
				  <div class="input-field col s12">
				  	<a class="btn cyan waves-effect waves-light right"  href="{{URL('/Idea')}}">Send Suggestion</a>
					<!--<button class="btn cyan waves-effect waves-light right" type="submit" name="action">Send Suggestion
					  <i class="mdi-content-send right"></i>
					</button>-->
				  </div>
				</div>
			  </div>
			</form>
		</div>
	</div>
	</div>
	</div>
</div>

@endsection
