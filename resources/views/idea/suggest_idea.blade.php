@extends('layouts.mind')

@section('form')

<div class="section">
	<div class="row">
	<div class="col s12 m5 l8 offset-l2

	 ">
	<div id="contact-page" class="card">
		<div class="card-content">
			<h4 class="header">Suggestion for edit</h4>
			<form class="contact-form">
			  <div class="row">
				<div class="input-field col s12">
				  <input id="name" type="text" class="validate" length="255" maxlength="255">
				  <label for="title">Title</label>
				</div>
			  </div>
			  <div class="row">
				<div class="input-field col s12">
				  <textarea id="content" class="materialize-textarea" length="5000" maxlength="5000"></textarea>
				  <label for="content">Content</label>
				</div>
				<div class="row">
				  <div class="input-field col s12">
				  	<a class="btn cyan waves-effect waves-light right"  href="{{URL('/Idea')}}">Send Suggestion</a>
					<!--<button class="btn cyan waves-effect waves-light right" type="submit" name="action">Send Suggestion
					  <i class="mdi-content-send right"></i>
					</button>-->
				  </div>
				</div>
			  </div>
			</form>
		</div>
	</div>
	</div>
	</div>
</div>

@endsection
