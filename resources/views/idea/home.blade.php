@extends('layouts.mind')

@section('Profile')
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                            </div>
                            <div class="col col s8 m8 l8">
                                <a class="btn-flat waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">{{Auth::user()->name}}</a>
                                <p class="user-roal">Registered user</p>
                            </div>
                        </div>
                    </li>
@endsection

@section('menu')
                <!-- User Login START -->
                <li class="li-hover"><div class="divider"></div></li>
				<li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
				</li>
				<li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
				</li>
				<li><a href="{{ route('logout') }}"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
				</li>
				<li class="li-hover"><div class="divider"></div></li>
                <li class="li-hover"><p class="ultra-small margin more-text">Idea</p></li>
				<li><a href="#"><i class="mdi-action-assignment-returned"></i> Suggestions</a>
				</li>
				<li><a href="#"><i class="mdi-action-stars"></i> Subscriptions</a>
				</li>
				<!-- User Login END -->
@endsection

@section('ideas') 
<div class="row">
                <div id="email-list" class="col s10 m4 l5 offset-l1 card-panel z-depth-1">
                  <ul class="collection">
                    <li class="collection-item">
                    <h6>Hot Ideas</h6>
                    </li>
                    <?php $count = 1 ?>
                    @foreach ($ideas as $idea)
                        <?php
                            $x = rand(0,4);
                            $Found = 0;
                            $color = array('circle grey darken-1', 'circle indigo darken-1', 'circle light-blue', 'circle light-blue', 'circle red');
                        ?>
                        @if(($idea -> status == 0 or $idea -> user_id ==  Auth::id() or $idea -> deleted_at == NULL) and $count <=3)
                        <li class="collection-item avatar email-unread">
                            <a href="{{URL::to('/Idea/' . $idea->id . '/relations')}}" class="email-unread">
                            <?php $x = rand(0,4); ?>
                            <span class="{{$color[$x]}}">{{$idea -> user -> name[0]}}</span>
                            <span class="email-title">{{$idea -> user -> name}}</span>
                            <div class="divider"></div>
                            <p class="truncate bold">{{$idea -> title}}</p>
                            <p class="truncate grey-text ultra-small">{{$idea -> content}}</p>
                            @foreach ($claps as $clap)
                            @if ($clap -> clapTo == $idea -> id)
                            @if ($clap -> user_id ==  Auth::id())
                            <?php  $Found = $Found+1?>
                            @endif
                            @endif
                            @endforeach
                            <?php if ($Found > 0): ?>
                              <a href="{{ URL::to('/Idea/' . $idea->id . '/clap') }}" class="secondary-content email-time">
                                  <span class="Claps badge red">{{$idea -> claps}}</span>
                              </a>
                            <?php endif; ?>
                            <?php if ($Found == 0): ?>
                              <a href="{{ URL::to('/Idea/' . $idea->id . '/clap') }}" class="secondary-content email-time">
                                  <span class="Claps badge blue">{{$idea -> claps}}</span>
                              </a>
                            <?php endif; ?>
                        </li>
                        @endif
                        <?php $count = $count + 1; ?>
                    @endforeach
                  </ul>
                </div>

                <div id="email-list" class="col s8 m4 l5 card-panel z-depth-1">
                  <ul class="collection">
                    <li class="collection-item">
                    <h6>Recent Ideas</h6>
                    </li>
                    <?php $count = 1?>
                    @foreach ($ideasDate as $idea)
                        <?php
                            $x = rand(0,4);
                            $Found = 0;
                            $color = array('circle grey darken-1', 'circle indigo darken-1', 'circle light-blue', 'circle light-blue', 'circle red');
                        ?>
                        <li class="collection-item avatar email-unread">
                            <a href=" {{URL::to('/Idea/' . $idea->id . '/relations')}}" class="email-unread">
                            <?php $x = rand(0,4); ?>
                            <span class="{{$color[$x]}}">{{$idea -> user -> name[0]}}</span>
                            <span class="email-title">{{$idea -> user -> name}}</span>
                            <div class="divider"></div>
                            <p class="truncate bold">{{$idea -> title}}</p>
                            <p class="truncate grey-text ultra-small">{{$idea -> created_at}}</p>
                            <div class="divider"></div>
                            <p class="truncate grey-text ultra-small">{{$idea -> content}}</p>

                            @foreach ($claps as $clap)
                            @if ($clap -> clapTo == $idea -> id)
                            @if ($clap -> user_id ==  Auth::id())
                            <?php  $Found = $Found+1?>
                            @endif
                            @endif
                            @endforeach
                            <?php if ($Found > 0): ?>
                              <a href="{{ URL::to('/Idea/' . $idea->id . '/clap') }}" class="secondary-content email-time">
                                  <span class="Claps badge red">{{$idea -> claps}}</span>
                              </a>
                            <?php endif; ?>
                            <?php if ($Found == 0): ?>
                              <a href="{{ URL::to('/Idea/' . $idea->id . '/clap') }}" class="secondary-content email-time">
                                  <span class="Claps badge blue">{{$idea -> claps}}</span>
                              </a>
                            <?php endif; ?>
                        </li>
                    @endforeach
                  </ul>
                </div>
</div>
@endsection
