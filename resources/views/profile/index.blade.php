<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Ideas Mapping</title>

    <!-- Favicons-->
    <link rel="icon" href="{{URL::asset('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{URL::asset('images/favicon/apple-touch-icon-152x152.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="{{URL::asset('css/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('css/style.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS for full screen (Layout-2)-->
    <link href="{{URL::asset('css/layouts/style-fullscreen.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{URL::asset('css/custom/custom.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{URL::asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('js/plugins/jvectormap/jquery-jvectormap.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('js/plugins/chartist-js/chartist.min.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body class="layout-light" onload="init()">
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    @if (Route::has('login'))
                    <ul class="left">
                      <li><h1 class="logo-wrapper"><a href="{{url('/')}}" class="brand-logo darken-1">
                        <img src="{{URL::asset('images/header-putih.png')}}" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore IdeasMapping"/>
                    </div>
                    @auth
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      </li>
                    </ul>
                    @endauth
                    @endif
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav leftside-navigation">
                    @yield('Profile')
                    @if (Route::has('login'))
                    @auth
                    <li class="bold"><a href="{{url('/Idea')}}" class="waves-effect waves-cyan">
                        <i class="mdi-action-dashboard"></i> Home</a>
                    </li>
                    @else
                    <li class="bold"><a href="{{url('/')}}" class="waves-effect waves-cyan">
                        <i class="mdi-action-dashboard"></i> Home</a>
                    </li>
                    @endauth
                    @endif


                @yield('menu')

                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">

          <!--start container-->
          <div class="container">

            <div id="profile-page" class="section">
              <!-- profile-page-header -->
              <div id="profile-page-header" class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                      <img class="activator" src="{{URL::asset('images/user-profile-bg.jpg')}}" alt="user background">
                  </div>
                  <figure class="card-profile-image">
                      <img src="{{URL::asset('images/avatar.jpg')}}" alt="profile image" class="circle z-depth-2 responsive-img activator">
                  </figure>
                  <div class="card-content">
                    <div class="row">
                      <div class="col s3 offset-s2">
                          <h4 class="card-title grey-text text-darken-4">Roger Waters</h4>
                      </div>
                      <div class="col s2 center-align">
                          <h4 class="card-title grey-text text-darken-4">10</h4>
                          <p class="medium-small grey-text">Ideas</p>
                      </div>
                      <div class="col s2 center-align">
                          <h4 class="card-title grey-text text-darken-4">6</h4>
                          <p class="medium-small grey-text">Subscriber</p>
                      </div>
                      <div class="col s2 center-align">
                          <h4 class="card-title grey-text text-darken-4">10</h4>
                          <p class="medium-small grey-text">Subscribes</p>
                      </div>
                      <div class="col s1 right-align">
                        <a class="btn-floating activator waves-effect waves-light darken-2 right">
                            <i class="mdi-action-perm-identity"></i>
                        </a>
                      </div>
                    </div>
                  </div>

                  <!-- Reveal Profile Info Start -->
                  <div class="card-reveal">
                      <p>
                        <span class="card-title grey-text text-darken-4">Roger Waters <i class="mdi-navigation-close right"></i></span>
                        <span><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> Project Manager</span>
                      </p>

                      <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>

                      <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +1 (612) 222 8989</p>
                      <p><i class="mdi-communication-email cyan-text text-darken-2"></i> mail@domain.com</p>
                      <p><i class="mdi-social-cake cyan-text text-darken-2"></i> 18th June 1990</p>
                      <p><i class="mdi-device-airplanemode-on cyan-text text-darken-2"></i> BAR - AUS</p>
                  </div>
                  <!-- Reveal Profile Info END -->
              </div>
              <!--/ profile-page-header -->

              <!-- profile-page-content -->
              <div id="profile-page-content" class="row">
                <!-- profile-page-wall -->
                <div id="profile-page-wall" class="col s12 m12">
                  <!-- profile-page-wall-posts -->
                  <div id="profile-page-wall-posts"class="row">
                    <div class="col s12">
                        <!-- medium -->
                        <div id="profile-page-wall-post" class="card">
                          <div class="card-profile-title">
                            <div class="row">
                              <div class="col s1">
                                <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-post-uer-image">
                              </div>
                              <div class="col s10">
                                <p class="grey-text text-darken-4 margin">John Doe</p>
                                <span class="grey-text text-darken-1 ultra-small">Shared publicly  -  26 Jun 2015</span>
                              </div>
                              <div class="col s1 right-align">
                                <i class="mdi-navigation-expand-more"></i>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <p>I am a very simple wall post. I am good at containing <a href="#">#small</a> bits of <a href="#">#information</a>.  I require little more information to use effectively.</p>
                              </div>
                            </div>
                          </div>
                          <div class="card-image profile-medium">
                            <img src="{{URL::asset('images/gallary/2.jpg')}}" alt="sample" class="responsive-img profile-post-image profile-medium">
                            <span class="card-title">Card Title</span>
                          </div>
                          <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action row">
                            <div class="col s4 card-action-share">
                              <a href="#">Like</a>
                              <a href="#">Share</a>
                            </div>

                            <div class="input-field col s8 margin">
                              <input id="profile-comments" type="text" class="validate margin">
                              <label for="profile-comments" class="">Comments</label>
                            </div>
                          </div>
                        </div>

                        <!-- medium video -->
                        <div id="profile-page-wall-post" class="card">
                          <div class="card-profile-title">
                            <div class="row">
                              <div class="col s1">
                                <img src="{{URL::asset('images/avatar.jpg')}}" alt="" class="circle responsive-img valign profile-post-uer-image">
                              </div>
                              <div class="col s10">
                                <p class="grey-text text-darken-4 margin">John Doe</p>
                                <span class="grey-text text-darken-1 ultra-small">Shared publicly  -  26 Jun 2015</span>
                              </div>
                              <div class="col s1 right-align">
                                <i class="mdi-navigation-expand-more"></i>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <p>I am a very simple wall post. I am good at containing <a href="#">#small</a> bits of <a href="#">#information</a>.  I require little more information to use effectively.</p>
                              </div>
                            </div>
                          </div>
                          <div class="card-image profile-medium">
                            <div class="video-container no-controls">
                              <iframe width="640" height="360" src="https://www.youtube.com/embed/10r9ozshGVE" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <span class="card-title">Card Title</span>
                          </div>
                          <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action row">
                            <div class="col s4 card-action-share">
                              <a href="#">Like</a>
                              <a href="#">Share</a>
                            </div>

                            <div class="input-field col s8 margin">
                              <input id="profile-comments" type="text" class="validate margin">
                              <label for="profile-comments" class="">Comments</label>
                            </div>
                          </div>
                        </div>

                        <!-- small -->
                        <div id="profile-page-wall-post" class="card">
                          <div class="card-profile-title">
                            <div class="row">
                              <div class="col s1">
                                <img src="{{URL::asset('images/avatar.jpg')}}" alt="" class="circle responsive-img valign profile-post-uer-image">
                              </div>
                              <div class="col s10">
                                <p class="grey-text text-darken-4 margin">John Doe</p>
                                <span class="grey-text text-darken-1 ultra-small">Shared publicly  -  26 Jun 2015</span>
                              </div>
                              <div class="col s1 right-align">
                                <i class="mdi-navigation-expand-more"></i>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <p>I am a very simple wall post. I am good at containing <a href="#">#small</a> bits of <a href="#">#information</a>.  I require little more information to use effectively.</p>
                              </div>
                            </div>
                          </div>
                          <div class="card-image profile-small">
                            <img src="{{URL::asset('images/gallary/1.jpg')}}" alt="sample" class="responsive-img profile-post-image">
                            <span class="card-title">Card Title</span>
                          </div>
                          <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action row">
                            <div class="col s4 card-action-share">
                              <a href="#">Like</a>
                              <a href="#">Share</a>
                            </div>

                            <div class="input-field col s8 margin">
                              <input id="profile-comments" type="text" class="validate">
                              <label for="profile-comments" class="">Comments</label>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <!--/ profile-page-wall-posts -->

                </div>
                <!--/ profile-page-wall -->

              </div>
            </div>
          </div>
          </div>
          <!--end container-->
        </section>
        <!-- END CONTENT -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START RIGHT SIDEBAR NAV-->
        <aside id="right-sidebar-nav">
          <ul id="chat-out" class="side-nav rightside-navigation">
              <li class="li-hover">
              <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
              <div id="right-search" class="row">
                  <form class="col s12">
                      <div class="input-field">
                          <i class="mdi-action-search prefix"></i>
                          <input id="icon_prefix" type="text" class="validate">
                          <label for="icon_prefix">Search</label>
                      </div>
                  </form>
              </div>
              </li>
              <li class="li-hover">
                  <ul class="chat-collapsible" data-collapsible="expandable">
                  <li>
                      <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                      <div class="collapsible-body recent-activity">
                          <div class="recent-activity-list chat-out-list row">
                              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                              </div>
                              <div class="col s9 recent-activity-list-text">
                                  <a href="#">just now</a>
                                  <p>Jim Doe Purchased new equipments for zonal office.</p>
                              </div>
                          </div>
                          <div class="recent-activity-list chat-out-list row">
                              <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                              </div>
                              <div class="col s9 recent-activity-list-text">
                                  <a href="#">Yesterday</a>
                                  <p>Your Next flight for USA will be on 15th August 2015.</p>
                              </div>
                          </div>
                          <div class="recent-activity-list chat-out-list row">
                              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                              </div>
                              <div class="col s9 recent-activity-list-text">
                                  <a href="#">5 Days Ago</a>
                                  <p>Natalya Parker Send you a voice mail for next conference.</p>
                              </div>
                          </div>
                          <div class="recent-activity-list chat-out-list row">
                              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                              </div>
                              <div class="col s9 recent-activity-list-text">
                                  <a href="#">Last Week</a>
                                  <p>Jessy Jay open a new store at S.G Road.</p>
                              </div>
                          </div>
                          <div class="recent-activity-list chat-out-list row">
                              <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                              </div>
                              <div class="col s9 recent-activity-list-text">
                                  <a href="#">5 Days Ago</a>
                                  <p>Natalya Parker Send you a voice mail for next conference.</p>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                      <div class="collapsible-body sales-repoart">
                          <div class="sales-repoart-list  chat-out-list row">
                              <div class="col s8">Target Salse</div>
                              <div class="col s4"><span id="sales-line-1"></span>
                              </div>
                          </div>
                          <div class="sales-repoart-list chat-out-list row">
                              <div class="col s8">Payment Due</div>
                              <div class="col s4"><span id="sales-bar-1"></span>
                              </div>
                          </div>
                          <div class="sales-repoart-list chat-out-list row">
                              <div class="col s8">Total Delivery</div>
                              <div class="col s4"><span id="sales-line-2"></span>
                              </div>
                          </div>
                          <div class="sales-repoart-list chat-out-list row">
                              <div class="col s8">Total Progress</div>
                              <div class="col s4"><span id="sales-bar-2"></span>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                      <div class="collapsible-body favorite-associates">
                          <div class="favorite-associate-list chat-out-list row">
                              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                              </div>
                              <div class="col s8">
                                  <p>Eileen Sideways</p>
                                  <p class="place">Los Angeles, CA</p>
                              </div>
                          </div>
                          <div class="favorite-associate-list chat-out-list row">
                              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                              </div>
                              <div class="col s8">
                                  <p>Zaham Sindil</p>
                                  <p class="place">San Francisco, CA</p>
                              </div>
                          </div>
                          <div class="favorite-associate-list chat-out-list row">
                              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                              </div>
                              <div class="col s8">
                                  <p>Renov Leongal</p>
                                  <p class="place">Cebu City, Philippines</p>
                              </div>
                          </div>
                          <div class="favorite-associate-list chat-out-list row">
                              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                              </div>
                              <div class="col s8">
                                  <p>Weno Carasbong</p>
                                  <p>Tokyo, Japan</p>
                              </div>
                          </div>
                          <div class="favorite-associate-list chat-out-list row">
                              <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                              </div>
                              <div class="col s8">
                                  <p>Nusja Nawancali</p>
                                  <p class="place">Bangkok, Thailand</p>
                              </div>
                          </div>
                      </div>
                  </li>
                  </ul>
              </li>
          </ul>
        </aside>
        <!-- LEFT RIGHT SIDEBAR NAV-->

      </div>
      <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->



    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2015 <a class="grey-text text-lighten-4" href="http://themeforest.net/user/geekslabs/portfolio?ref=geekslabs" target="_blank">GeeksLabs</a> All rights reserved.
                <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="{{url::asset('js/plugins/jquery-1.11.2.min.js')}}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{url::asset('js/materialize.min.js')}}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{url::asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="{{url::asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/sparkline/sparkline-script.js')}}"></script>

    <!--jvectormap-->
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/vectormap-script.js')}}"></script>


    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{url::asset('js/plugins.min.js')}}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{url::asset('js/custom-script.js')}}"></script>
</body>

</html>
