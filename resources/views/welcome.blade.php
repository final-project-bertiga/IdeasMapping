@extends('layouts.mind')

@section('Profile')
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                            </div>
                            <div class="col col s8 m8 l8">
                                <a class="btn-flat waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">User</a>
                                <p class="user-roal">Non registered user</p>
                            </div>
                        </div>
                    </li>
@endsection

@section('menu')
                <!-- Not logged in START -->
                <li class="li-hover"><div class="divider"></div></li>
                <li><a href="{{ route('login') }}"><i class="mdi-action-verified-user"></i>Login</a>
                </li>
                <li><a href="{{ route('register') }}"><i class="mdi-action-account-circle"></i>Register</a>
                </li>
                <li><a href="{{url('password/reset')}}"><i class="mdi-communication-live-help"></i>Forgot Password</a>
                </li>
                <!-- Not logged in END -->
@endsection

@section('ideas')
                <div id="email-list" class="col s10 m4 l5 offset-l1 card-panel z-depth-1">
                  <ul class="collection">
                    <li class="collection-item">
                    <h6>Hot Ideas</h6>
                    </li>
                    <?php $count = 1 ?>
                    @foreach ($ideas as $idea)
                        <?php
                            $x = rand(0,4);
                            $color = array('circle grey darken-1', 'circle indigo darken-1', 'circle light-blue', 'circle light-blue', 'circle red');
                        ?>
                        @if(($idea -> status == 0 or $idea -> user_id ==  Auth::id() or $idea -> deleted_at == NULL) and $count <=3)
                        <li class="collection-item avatar email-unread"><a href="#ToTheIdea" class="email-unread">
                            <?php $x = rand(0,4); ?>
                            <span class="{{$color[$x]}}">{{$idea -> name[0]}}</span>
                            <span class="email-title">{{$idea -> name}}</span>
                            <div class="divider"></div>
                            <p class="truncate bold">{{$idea -> title}}</p>
                            <p class="truncate grey-text ultra-small">{{$idea -> content}}</p>
                            <a href="{{ route('login') }}" class="secondary-content email-time">
                                <span class="Claps badge blue">{{$idea -> claps}}</span>
                            </a>
                        </li>
                        @endif
                        <?php $count = $count + 1; ?>
                    @endforeach
                  </ul>
                </div>

                <div id="email-list" class="col s8 m4 l5 card-panel z-depth-1">
                  <ul class="collection">
                    <li class="collection-item">
                    <h6>Recent Ideas</h6>
                    </li>
                    @foreach ($ideasDate as $idea)
                        <?php
                            $x = rand(0,4);
                            $color = array('circle grey darken-1', 'circle indigo darken-1', 'circle light-blue', 'circle light-blue', 'circle red');
                        ?>
                        <li class="collection-item avatar email-unread"><a href="#!link" class="email-unread">
                            <?php $x = rand(0,4); ?>
                            <span class="{{$color[$x]}}">{{$idea -> name[0]}}</span>
                            <span class="email-title">{{$idea -> name}}</span>
                            <div class="divider"></div>
                            <p class="truncate bold">{{$idea -> title}}</p>
                            <p class="truncate grey-text ultra-small">{{$idea -> content}}</p>
                            <a href="{{ route('login') }}" class="secondary-content email-time">
                                <span class="Claps badge blue">{{$idea -> claps}}</span>
                            </a>
                        </li>
                    @endforeach
                  </ul>
                </div>
@endsection
