<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Ideas Mapping</title>

    <!-- Favicons-->
    <link rel="icon" href="{{URL::asset('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{URL::asset('images/favicon/apple-touch-icon-152x152.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="{{URL::asset('css/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('css/style.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS for full screen (Layout-2)-->
    <link href="{{URL::asset('css/layouts/style-fullscreen.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{URL::asset('css/custom/custom.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{URL::asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('js/plugins/jvectormap/jquery-jvectormap.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{URL::asset('js/plugins/chartist-js/chartist.min.css')}}"
        type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body class="layout-light" onload="init()">
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color">
                <div class="nav-wrapper">
                    @if (Route::has('login'))
                    <ul class="left">
                      <li><h1 class="logo-wrapper"><a href="{{url('/')}}" class="brand-logo darken-1">
                        <img src="{{URL::asset('images/header-putih.png')}}" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="mdi-action-search"></i>
                        <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore IdeasMapping"/>
                    </div>
                    @auth
                    <ul class="right hide-on-med-and-down">
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i></a>
                        </li>
                    </ul>
                    <!-- notifications-dropdown -->
                    <ul id="notifications-dropdown" class="dropdown-content">
                      <li>
                        <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                      </li>
                      <li>
                        <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                      </li>
                      </li>
                    </ul>
                    @endauth
                    @endif
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav leftside-navigation">
                    @yield('Profile')
                    @if (Route::has('login'))
                    @auth
                    <li class="bold"><a href="{{url('/Idea')}}" class="waves-effect waves-cyan">
                        <i class="mdi-action-dashboard"></i> Home</a>
                    </li>
                    @else
                    <li class="bold"><a href="{{url('/')}}" class="waves-effect waves-cyan">
                        <i class="mdi-action-dashboard"></i> Home</a>
                    </li>
                    @endauth
                    @endif


                @yield('menu')

                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">
                    @yield('ideaPage')
                    @yield('ideas')
                    @if (Route::has('login'))
                    @auth
                    @yield('form')
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!-- Create Idea Trigger -->
					<div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
					  <a class="btn-floating btn-large red modal-trigger" href="#modal1">
						<i class="mdi-editor-border-color"></i>
					  </a>
					</div>
                    <!-- Create Idea Trigger -->

					<!-- Create Idea Structure Start -->
					<div id="modal1" class="modal">
					  <div class="modal-content">
						<nav class="red">
						  <div class="nav-wrapper">
							<div class="left col s12 m5 l5">
							  <ul>
								<li><a href="#!" class="email-menu"><i class="modal-action modal-close  mdi-hardware-keyboard-backspace"></i></a>
								</li>
								<li><a href="#!" class="email-type">Create Idea</a>
								</li>
							  </ul>
							</div>
              <div class="col s12 m7 l7 hide-on-med-and-down">
							  <ul class="right">
								<li><a href="#" onclick="document.getElementById('create-idea').submit();"><i class="modal-action modal-close  mdi-content-send"></i></a>
								</li>
							  </ul>
							</div>

						  </div>
						</nav>
					  </div>
            <div class="model-email-content">
						<div class="row">
            <form id="create-idea" class="col s12" action="/Idea" method="post">
    					{{ csrf_field() }}
							<div class="row">
							  <div class="input-field col s12">
								<input id="title" name="title" type="text" class="validate" length="255" maxlength="255">
								<label for="title">Title</label>
                <input type="hidden" name="user_id" id="user_id" value="{{Auth::id()}}">
							  </div>
							</div>
							<div class="row">
							  <div class="input-field col s12">
								<textarea name="content" id="compose" class="materialize-textarea" length="5000" maxlength="5000"></textarea>
								<label for="compose">Content</label>
							  </div>
							</div>
              <div class="col s12 m7 l7 hide-on-med-and-down">
							  <ul class="right">
							  </ul>
							</div>
						  </form>
						</div>
					  </div>

					</div>
					<!-- Create Idea Structure END -->
          @endauth
          @endif

                </div>
                <!--end container-->
            </section>
            @yield('rightSideNav')
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->



    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2018 <a class="grey-text text-lighten-4" href="http://nusantaracreative.com/" target="_blank">Nusantara Creative</a> All rights reserved.
                <span class="right"> Design by <a class="grey-text text-lighten-4" href="http://nusantaracreative.com/">Nusantara Creative</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="{{url::asset('js/plugins/jquery-1.11.2.min.js')}}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{url::asset('js/materialize.min.js')}}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{url::asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="{{url::asset('js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/sparkline/sparkline-script.js')}}"></script>

    <!--jvectormap-->
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script type="text/javascript" src="{{url::asset('js/plugins/jvectormap/vectormap-script.js')}}"></script>


    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{url::asset('js/plugins.min.js')}}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{url::asset('js/custom-script.js')}}"></script>

    <!-- Gojs -->
		<script type="text/javascript" src="https://gojs.net/latest/release/go.js"></script>
	    <script>
	     function init() {
	    	var $ = go.GraphObject.make;

	    	myDiagram =
	      $(go.Diagram, "map-canvas",
	        {
	          padding: 20,
	          // when the user drags a node, also move/copy/delete the whole subtree starting with that node
	          "draggingTool.dragsTree": false,
	          initialContentAlignment: go.Spot.Center,  // center the whole graph
	        });

	    	// a node consists of some text with a line shape underneath
	    	myDiagram.nodeTemplate =
	      $(go.Node, "Vertical",
	        { selectionObjectName: "TEXT" },
	        $(go.TextBlock,
	          {
	            name: "TEXT",
	            minSize: new go.Size(30, 15),
	            editable: true
	          },
	          // remember not only the text string but the scale and the font in the node data
	          new go.Binding("text", "text").makeTwoWay(),
	          new go.Binding("scale", "scale").makeTwoWay(),
	          new go.Binding("font", "font").makeTwoWay()),
	        $(go.Shape, "LineH",
	          {
	            stretch: go.GraphObject.Horizontal,
	            strokeWidth: 3, height: 3,
	            // this line shape is the port -- what links connect with
	            portId: "", fromSpot: go.Spot.LeftRightSides, toSpot: go.Spot.LeftRightSides
	          },
	          new go.Binding("stroke", "brush"),
	          // make sure links come in from the proper direction and go out appropriately
	          new go.Binding("fromSpot", "dir", function(d) { return spotConverter(d, true); }),
	          new go.Binding("toSpot", "dir", function(d) { return spotConverter(d, false); })),
	        // remember the locations of each node in the node data
	        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
	        // make sure text "grows" in the desired direction
	        new go.Binding("locationSpot", "dir", function(d) { return spotConverter(d, false); })
	      );

		    // the context menu allows users to change the font size and weight,
		    // and to perform a limited tree layout starting at that node
		    myDiagram.nodeTemplate.contextMenu =
		      $(go.Adornment, "Vertical",
		        $("ContextMenuButton",
		          $(go.TextBlock, "Bigger"),
		          { click: function(e, obj) { changeTextSize(obj, 1.1); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Smaller"),
		          { click: function(e, obj) { changeTextSize(obj, 1/1.1); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Bold/Normal"),
		          { click: function(e, obj) { toggleTextWeight(obj); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Layout"),
		          {
		            click: function(e, obj) {
		                var adorn = obj.part;
		                adorn.diagram.startTransaction("Subtree Layout");
		                layoutTree(adorn.adornedPart);
		                adorn.diagram.commitTransaction("Subtree Layout");
		              }
		          }
		        )
		      );

		    // a link is just a Bezier-curved line of the same color as the node to which it is connected
		    myDiagram.linkTemplate =
		      $(go.Link,
		        {
		          curve: go.Link.Bezier,
		          fromShortLength: -2,
		          toShortLength: -2,
		          selectable: false
		        },
		        $(go.Shape,
		          { strokeWidth: 3 },
		          new go.Binding("stroke", "toNode", function(n) {
		            if (n.data.brush) return n.data.brush;
		            return "black";
		          }).ofObject())
		      );

		    myDiagram.addDiagramListener("SelectionMoved", function(e) {
		      var rootX = myDiagram.findNodeForKey(0).location.x;
		      myDiagram.selection.each(function(node) {
		          if (node.data.parent !== 0) return; // Only consider nodes connected to the root
		          var nodeX = node.location.x;
		          if (rootX < nodeX && node.data.dir !== "right") {
		            updateNodeDirection(node, "right");
		          } else if (rootX > nodeX && node.data.dir !== "left") {
		            updateNodeDirection(node, "left");
		          }
		          layoutTree(node);
		        });
		    });

	    	// read in the predefined graph using the JSON format data held in the "mySavedModel" textarea
	    	load();
	  	}

	  function spotConverter(dir, from) {
	    if (dir === "left") {
	      return (from ? go.Spot.Left : go.Spot.Right);
	    } else {
	      return (from ? go.Spot.Right : go.Spot.Left);
	    }
	  }

	  function changeTextSize(obj, factor) {
	    var adorn = obj.part;
	    adorn.diagram.startTransaction("Change Text Size");
	    var node = adorn.adornedPart;
	    var tb = node.findObject("TEXT");
	    tb.scale *= factor;
	    adorn.diagram.commitTransaction("Change Text Size");
	  }

	  function toggleTextWeight(obj) {
	    var adorn = obj.part;
	    adorn.diagram.startTransaction("Change Text Weight");
	    var node = adorn.adornedPart;
	    var tb = node.findObject("TEXT");
	    // assume "bold" is at the start of the font specifier
	    var idx = tb.font.indexOf("bold");
	    if (idx < 0) {
	      tb.font = "bold " + tb.font;
	    } else {
	      tb.font = tb.font.substr(idx + 5);
	    }
	    adorn.diagram.commitTransaction("Change Text Weight");
	  }

	  function updateNodeDirection(node, dir) {
	    myDiagram.model.setDataProperty(node.data, "dir", dir);
	    // recursively update the direction of the child nodes
	    var chl = node.findTreeChildrenNodes(); // gives us an iterator of the child nodes related to this particular node
	    while(chl.next()) {
	      updateNodeDirection(chl.value, dir);
	    }
	  }

	  function addNodeAndLink(e, obj) {
	    var adorn = obj.part;
	    var diagram = adorn.diagram;
	    diagram.startTransaction("Add Node");
	    var oldnode = adorn.adornedPart;
	    var olddata = oldnode.data;
	    // copy the brush and direction to the new node data
	    var newdata = { text: "idea", brush: olddata.brush, dir: olddata.dir, parent: olddata.key };
	    diagram.model.addNodeData(newdata);
	    layoutTree(oldnode);
	    diagram.commitTransaction("Add Node");

	    // if the new node is off-screen, scroll the diagram to show the new node
	    var newnode = diagram.findNodeForData(newdata);
	    if (newnode !== null) diagram.scrollToRect(newnode.actualBounds);
	  }

	  function layoutTree(node) {
	    if (node.data.key === 0) {  // adding to the root?
	      layoutAll();  // lay out everything
	    } else {  // otherwise lay out only the subtree starting at this parent node
	      var parts = node.findTreeParts();
	      layoutAngle(parts, node.data.dir === "left" ? 180 : 0);
	    }
	  }

	  function layoutAngle(parts, angle) {
	    var layout = go.GraphObject.make(go.TreeLayout,
	        { angle: angle,
	          arrangement: go.TreeLayout.ArrangementFixedRoots,
	          nodeSpacing: 5,
	          layerSpacing: 20,
	          setsPortSpot: false, // don't set port spots since we're managing them with our spotConverter function
	          setsChildPortSpot: false });
	    layout.doLayout(parts);
	  }

	  function layoutAll() {
	    var root = myDiagram.findNodeForKey(0);
	    if (root === null) return;
	    myDiagram.startTransaction("Layout");
	    // split the nodes and links into two collections
	    var rightward = new go.Set(go.Part);
	    var leftward = new go.Set(go.Part);
	    root.findLinksConnected().each(function(link) {
	        var child = link.toNode;
	        if (child.data.dir === "left") {
	          leftward.add(root);  // the root node is in both collections
	          leftward.add(link);
	          leftward.addAll(child.findTreeParts());
	        } else {
	          rightward.add(root);  // the root node is in both collections
	          rightward.add(link);
	          rightward.addAll(child.findTreeParts());
	        }
	      });
	    // do one layout and then the other without moving the shared root node
	    layoutAngle(rightward, 0);
	    layoutAngle(leftward, 180);
	    myDiagram.commitTransaction("Layout");
	  }

	  // Show the diagram's model in JSON format
	  function save() {
	    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
	    myDiagram.isModified = false;
	  }
	  function load() {
	    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
	  }
	    </script>
</body>
</html>
