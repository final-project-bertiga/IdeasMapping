@extends('layouts.form')

@section('form')
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Register</h4>
          </div>
        </div>

        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
            <label for="name" class="center-align">Name</label>
          </div>
        </div>
        @if ($errors->has('name'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
          </i>{{ $errors->first('name') }}</p>
          </div>
        </div>
        @endif

        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required>
            <label for="email" class="center-align">Email</label>
          </div>
        </div>
        @if ($errors->has('email'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
          </i>{{ $errors->first('email') }}</p>
          </div>
        </div>
        @endif

        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password" required>
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password-confirm" type="password" name="password_confirmation" required>
            <label for="password-confirm">Password again</label>
          </div>
        </div>
        @if ($errors->has('password'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
          </i>{{ $errors->first('password') }}</p>
          </div>
        </div>
        @endif
        <div class="row">
          <div class="input-field col s12">
            <button type="submit" class="btn waves-effect waves-light col s12">Register Now</button>
          </div>
          <div class="input-field col s12">
            <p class="margin center medium-small sign-up">Already have an account? <a href="{{ route('login') }}">Login</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection
