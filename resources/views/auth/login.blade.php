@extends('layouts.form')

@section('form')
  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Login</h4>
          </div>
        </div>
        @if ($errors->has('email') or $errors->has('password'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
          </i>The e-mail address and/or password you specified are not correct.</p>
          </div>
        </div>
        @endif
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
            <label for="email" class="center-align">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password" required>
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 m12 l12  login-text">
              <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}/>
              <label for="remember">Remember me</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <center>
                <button type="submit" class="btn waves-effect waves-light col s12">
                    Login
                </button>
            </center>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="{{url('register')}}">Register Now!</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="{{url('password/reset')}}">Forgot password ?</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection
