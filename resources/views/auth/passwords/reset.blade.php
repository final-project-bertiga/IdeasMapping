<!DOCTYPE html>
<html lang="en">

<!--================================================================================
  Item Name: Materialize - Material Design Admin Template
  Version: 3.1
  Author: GeeksLabs
  Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Login Page</title>

  <!-- Favicons-->
  <link rel="icon" href="{{URL::asset('images/favicon/favicon-32x32.png')}}" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="{{URL::asset('/images/favicon/apple-touch-icon-152x152.png')}}">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="{{URL::asset('/images/favicon/mstile-144x144.png')}}">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  <link href="{{URL::asset('/css/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{URL::asset('/css/style.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  
  <!-- Custome CSS-->
  <link href="{{URL::asset('/css/custom/custom.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{URL::asset('/css/layouts/page-center.css')}}" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="{{URL::asset('/js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{URL::asset('/js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">

</head>

<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Reset Password</h4>
          </div>
        </div>
        @if ($errors->has('email'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
            </i>The e-mail address you specified are not correct.</p>
          </div>
        </div>
        @endif
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input type="hidden" name="token" value="{{ $token }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
            <label for="email" class="center-align">Email</label>
          </div>
        </div>

        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password" required>
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password-confirm" type="password" name="password_confirmation" required>
            <label for="password-confirm">Password again</label>
          </div>
        </div>
        @if ($errors->has('password'))
        <div id="card-alert" class="card red">
          <div class="card-content white-text">
          </i>{{ $errors->first('password') }}</p>
          </div>
        </div>
        @endif

        <div class="row">
          <div class="input-field col s12">
            <center>
                <button type="submit" class="btn waves-effect waves-light col s12">
                    Change password
                </button>
            </center>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="{{url('register')}}">Register Now!</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="{{url('login')}}">Login</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>


  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="{{URL::asset('/js/plugins/jquery-1.11.2.min.js')}}"></script>
  <!--materialize js-->
  <script type="text/javascript" src="{{URL::asset('/js/materialize.min.js')}}"></script>
  <!--prism-->
  <script type="text/javascript" src="{{URL::asset('/js/plugins/prism/prism.js')}}"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="{{URL::asset('/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{URL::asset('/js/plugins.min.js')}}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{URL::asset('/js/custom-script.js')}}"></script>

</body>

</html>

</html>