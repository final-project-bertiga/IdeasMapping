<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Materialize - Material Design Admin Template</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS for full screen (Layout-2)-->
    <link href="css/layouts/style-fullscreen.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


</head>


<body onload="init()">
	<!-- //////////////////////////////////////////////////////////////////////////// -->

	<!-- START HEADER -->
	<header id="header" class="page-topbar">
			<!-- start header nav-->
			<div class="navbar-fixed">
					<nav class="navbar-color">
							<div class="nav-wrapper">
									<ul class="left">
										<li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="images/logo-header.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
									</ul>
									<div class="header-search-wrapper hide-on-med-and-down">
											<i class="mdi-action-search"></i>
											<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore IdeasMapping"/>
									</div>
									<ul class="right hide-on-med-and-down">
											<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
											</li>
											<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i></a>
											</li>
									</ul>
									<!-- notifications-dropdown -->
									<ul id="notifications-dropdown" class="dropdown-content">
										<li>
											<h5>NOTIFICATIONS <span class="new badge">5</span></h5>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
											<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
										</li>
										<li>
											<a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
											<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
										</li>
										<li>
											<a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
											<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
										</li>
										<li>
											<a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
											<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
										</li>
										<li>
											<a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
											<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
										</li>
									</ul>
							</div>
					</nav>
			</div>
			<!-- end header nav-->
	</header>
	<!-- END HEADER -->

	<!-- //////////////////////////////////////////////////////////////////////////// -->

<div id="main">
	<div class="wrapper">

  <div id="myDiagramDiv" style="border: solid 1px black; width:100%; height:300px;"></div>
	</div>
</div>
  <textarea id="mySavedModel" style="width:100%;height:400px;display:none;" >{ "class": "go.TreeModel",
  "nodeDataArray": [
{"key":0, "text":"Mind Map", "loc":"0 0"},
{"key":1, "parent":0, "text":"Getting more time", "brush":"skyblue", "dir":"right", "loc":"77 -22"},
{"key":11, "parent":1, "text":"Wake up early", "brush":"skyblue", "dir":"right", "loc":"200 -48"},
{"key":12, "parent":1, "text":"Delegate", "brush":"skyblue", "dir":"right", "loc":"200 -22"},
{"key":13, "parent":1, "text":"Simplify", "brush":"skyblue", "dir":"right", "loc":"200 4"},
{"key":2, "parent":0, "text":"More effective use", "brush":"darkseagreen", "dir":"right", "loc":"77 43"},
{"key":21, "parent":2, "text":"Planning", "brush":"darkseagreen", "dir":"right", "loc":"203 30"},
{"key":211, "parent":21, "text":"Priorities", "brush":"darkseagreen", "dir":"right", "loc":"274 17"},
{"key":212, "parent":21, "text":"Ways to focus", "brush":"darkseagreen", "dir":"right", "loc":"274 43"},
{"key":22, "parent":2, "text":"Goals", "brush":"darkseagreen", "dir":"right", "loc":"203 56"},
{"key":3, "parent":0, "text":"Time wasting", "brush":"palevioletred", "dir":"left", "loc":"-20 -31.75"},
{"key":31, "parent":3, "text":"Too many meetings", "brush":"palevioletred", "dir":"left", "loc":"-117 -64.25"},
{"key":32, "parent":3, "text":"Too much time spent on details", "brush":"palevioletred", "dir":"left", "loc":"-117 -25.25"},
{"key":33, "parent":3, "text":"Message fatigue", "brush":"palevioletred", "dir":"left", "loc":"-117 0.75"},
{"key":331, "parent":31, "text":"Check messages less", "brush":"palevioletred", "dir":"left", "loc":"-251 -77.25"},
{"key":332, "parent":31, "text":"Message filters", "brush":"palevioletred", "dir":"left", "loc":"-251 -51.25"},
{"key":4, "parent":0, "text":"Key issues", "brush":"coral", "dir":"left", "loc":"-20 52.75"},
{"key":41, "parent":4, "text":"Methods", "brush":"coral", "dir":"left", "loc":"-103 26.75"},
{"key":42, "parent":4, "text":"Deadlines", "brush":"coral", "dir":"left", "loc":"-103 52.75"},
{"key":43, "parent":4, "text":"Checkpoints", "brush":"coral", "dir":"left", "loc":"-103 78.75"}
 ]
}
  </textarea>




	<!-- //////////////////////////////////////////////////////////////////////////// -->


		<!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2018 <a class="grey-text text-lighten-4" href="http://nusantaracreative.com/" target="_blank">Nusantara Creative</a> All rights reserved.
                <span class="right"> Design by <a class="grey-text text-lighten-4" href="http://nusantaracreative.com/">Nusantara Creative</a></span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!-- sparkline -->
    <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>

    <!--jvectormap-->
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>


    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>

		<!-- Gojs -->
		<script type="text/javascript" src="https://gojs.net/latest/release/go.js"></script>
	    <script>
	     function init() {
	    	var $ = go.GraphObject.make;

	    	myDiagram =
	      $(go.Diagram, "myDiagramDiv",
	        {
	          padding: 20,
	          // when the user drags a node, also move/copy/delete the whole subtree starting with that node
	          "draggingTool.dragsTree": false,
	          initialContentAlignment: go.Spot.Center,  // center the whole graph
	        });

	    	// a node consists of some text with a line shape underneath
	    	myDiagram.nodeTemplate =
	      $(go.Node, "Vertical",
	        { selectionObjectName: "TEXT" },
	        $(go.TextBlock,
	          {
	            name: "TEXT",
	            minSize: new go.Size(30, 15),
	            editable: true
	          },
	          // remember not only the text string but the scale and the font in the node data
	          new go.Binding("text", "text").makeTwoWay(),
	          new go.Binding("scale", "scale").makeTwoWay(),
	          new go.Binding("font", "font").makeTwoWay()),
	        $(go.Shape, "LineH",
	          {
	            stretch: go.GraphObject.Horizontal,
	            strokeWidth: 3, height: 3,
	            // this line shape is the port -- what links connect with
	            portId: "", fromSpot: go.Spot.LeftRightSides, toSpot: go.Spot.LeftRightSides
	          },
	          new go.Binding("stroke", "brush"),
	          // make sure links come in from the proper direction and go out appropriately
	          new go.Binding("fromSpot", "dir", function(d) { return spotConverter(d, true); }),
	          new go.Binding("toSpot", "dir", function(d) { return spotConverter(d, false); })),
	        // remember the locations of each node in the node data
	        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
	        // make sure text "grows" in the desired direction
	        new go.Binding("locationSpot", "dir", function(d) { return spotConverter(d, false); })
	      );

		    // the context menu allows users to change the font size and weight,
		    // and to perform a limited tree layout starting at that node
		    myDiagram.nodeTemplate.contextMenu =
		      $(go.Adornment, "Vertical",
		        $("ContextMenuButton",
		          $(go.TextBlock, "Bigger"),
		          { click: function(e, obj) { changeTextSize(obj, 1.1); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Smaller"),
		          { click: function(e, obj) { changeTextSize(obj, 1/1.1); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Bold/Normal"),
		          { click: function(e, obj) { toggleTextWeight(obj); } }),
		        $("ContextMenuButton",
		          $(go.TextBlock, "Layout"),
		          {
		            click: function(e, obj) {
		                var adorn = obj.part;
		                adorn.diagram.startTransaction("Subtree Layout");
		                layoutTree(adorn.adornedPart);
		                adorn.diagram.commitTransaction("Subtree Layout");
		              }
		          }
		        )
		      );

		    // a link is just a Bezier-curved line of the same color as the node to which it is connected
		    myDiagram.linkTemplate =
		      $(go.Link,
		        {
		          curve: go.Link.Bezier,
		          fromShortLength: -2,
		          toShortLength: -2,
		          selectable: false
		        },
		        $(go.Shape,
		          { strokeWidth: 3 },
		          new go.Binding("stroke", "toNode", function(n) {
		            if (n.data.brush) return n.data.brush;
		            return "black";
		          }).ofObject())
		      );

		    myDiagram.addDiagramListener("SelectionMoved", function(e) {
		      var rootX = myDiagram.findNodeForKey(0).location.x;
		      myDiagram.selection.each(function(node) {
		          if (node.data.parent !== 0) return; // Only consider nodes connected to the root
		          var nodeX = node.location.x;
		          if (rootX < nodeX && node.data.dir !== "right") {
		            updateNodeDirection(node, "right");
		          } else if (rootX > nodeX && node.data.dir !== "left") {
		            updateNodeDirection(node, "left");
		          }
		          layoutTree(node);
		        });
		    });

	    	// read in the predefined graph using the JSON format data held in the "mySavedModel" textarea
	    	load();
	  	}

	  function spotConverter(dir, from) {
	    if (dir === "left") {
	      return (from ? go.Spot.Left : go.Spot.Right);
	    } else {
	      return (from ? go.Spot.Right : go.Spot.Left);
	    }
	  }

	  function changeTextSize(obj, factor) {
	    var adorn = obj.part;
	    adorn.diagram.startTransaction("Change Text Size");
	    var node = adorn.adornedPart;
	    var tb = node.findObject("TEXT");
	    tb.scale *= factor;
	    adorn.diagram.commitTransaction("Change Text Size");
	  }

	  function toggleTextWeight(obj) {
	    var adorn = obj.part;
	    adorn.diagram.startTransaction("Change Text Weight");
	    var node = adorn.adornedPart;
	    var tb = node.findObject("TEXT");
	    // assume "bold" is at the start of the font specifier
	    var idx = tb.font.indexOf("bold");
	    if (idx < 0) {
	      tb.font = "bold " + tb.font;
	    } else {
	      tb.font = tb.font.substr(idx + 5);
	    }
	    adorn.diagram.commitTransaction("Change Text Weight");
	  }

	  function updateNodeDirection(node, dir) {
	    myDiagram.model.setDataProperty(node.data, "dir", dir);
	    // recursively update the direction of the child nodes
	    var chl = node.findTreeChildrenNodes(); // gives us an iterator of the child nodes related to this particular node
	    while(chl.next()) {
	      updateNodeDirection(chl.value, dir);
	    }
	  }

	  function addNodeAndLink(e, obj) {
	    var adorn = obj.part;
	    var diagram = adorn.diagram;
	    diagram.startTransaction("Add Node");
	    var oldnode = adorn.adornedPart;
	    var olddata = oldnode.data;
	    // copy the brush and direction to the new node data
	    var newdata = { text: "idea", brush: olddata.brush, dir: olddata.dir, parent: olddata.key };
	    diagram.model.addNodeData(newdata);
	    layoutTree(oldnode);
	    diagram.commitTransaction("Add Node");

	    // if the new node is off-screen, scroll the diagram to show the new node
	    var newnode = diagram.findNodeForData(newdata);
	    if (newnode !== null) diagram.scrollToRect(newnode.actualBounds);
	  }

	  function layoutTree(node) {
	    if (node.data.key === 0) {  // adding to the root?
	      layoutAll();  // lay out everything
	    } else {  // otherwise lay out only the subtree starting at this parent node
	      var parts = node.findTreeParts();
	      layoutAngle(parts, node.data.dir === "left" ? 180 : 0);
	    }
	  }

	  function layoutAngle(parts, angle) {
	    var layout = go.GraphObject.make(go.TreeLayout,
	        { angle: angle,
	          arrangement: go.TreeLayout.ArrangementFixedRoots,
	          nodeSpacing: 5,
	          layerSpacing: 20,
	          setsPortSpot: false, // don't set port spots since we're managing them with our spotConverter function
	          setsChildPortSpot: false });
	    layout.doLayout(parts);
	  }

	  function layoutAll() {
	    var root = myDiagram.findNodeForKey(0);
	    if (root === null) return;
	    myDiagram.startTransaction("Layout");
	    // split the nodes and links into two collections
	    var rightward = new go.Set(go.Part);
	    var leftward = new go.Set(go.Part);
	    root.findLinksConnected().each(function(link) {
	        var child = link.toNode;
	        if (child.data.dir === "left") {
	          leftward.add(root);  // the root node is in both collections
	          leftward.add(link);
	          leftward.addAll(child.findTreeParts());
	        } else {
	          rightward.add(root);  // the root node is in both collections
	          rightward.add(link);
	          rightward.addAll(child.findTreeParts());
	        }
	      });
	    // do one layout and then the other without moving the shared root node
	    layoutAngle(rightward, 0);
	    layoutAngle(leftward, 180);
	    myDiagram.commitTransaction("Layout");
	  }

	  // Show the diagram's model in JSON format
	  function save() {
	    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
	    myDiagram.isModified = false;
	  }
	  function load() {
	    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
	  }
	    </script>
</body>
</html>
