<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/gojs',function ()
{
    return view('gojs');
});

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('/Idea', 'ideacontroller');

Route::get('/Idea/{id}/clap', 'ideacontroller@addClaps');

Route::get('/Idea/{id}/permission', 'ideacontroller@permission');

Route::get('/Idea/{id}/relations', 'relationController@show');

Route::get('/Idea/{id}/relations/create', 'relationController@create');

Route::get('/Idea/{id}/delete', 'ideacontroller@destroy');

Route::get('/Idea/relations/{id}/accept', 'relationController@accept');

Route::get('/Idea/relations/{id}/decline', 'relationController@decline');

Route::get('/Idea/sugestion/{id}/accept', 'sugestionController@accept');

Route::get('/Idea/sugestion/{id}/decline', 'sugestionController@decline');

route::get('/Idea/sugestion/{id}/To', 'sugestionController@sugestion');

route::get('/notifications/{id}/','notificationController@index');

route::get('/Profile/{id}/edit', 'profileController@edit');

route::get('/Profile/{id}', 'profileController@index');

route::get('/Profile/{id}/subscribe', 'profileController@subs');
